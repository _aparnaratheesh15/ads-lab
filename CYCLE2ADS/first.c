#include<stdio.h>
#include<stdlib.h>
int universal_set[20],set1[20],set2[20];
int bitstring1[20],bitstring2[20],bitstringunion[20],bitstringintersection[20],bitstringdifference[20],universalbitstringset[20];
int isbitstring_ready=0,set1_size=0,set2_size=0,universalset_size=0;
void generatebitstring(int arr[],int size,int bitstringarray[]);
void printset(int arr[],int size);
int search(int arr[],int size,int elem);



void getset(int arr[],int size){
int i,j,k;
printf("\n Enter the set elements: ");
for(i=0;i<size;i++){
scanf("%d",&arr[i]);
}
for(i=0;i<size;i++)
{
    for(j=i+1;j<size;j++)
    {
        if(arr[i]==arr[j])
        {
            for(k=j;k<size-1;k++)
            {
                arr[k]=arr[k+1];
            }
            size--;
            j--;
            }
        }
    }
printf("set elements are:-");
for(i=0;i<size;i++)
{
    printf("%d",arr[i]);
}

}

void printbitstring(){
int i;
for(i=0;i<universalset_size;i++){
bitstring1[i]=0;
bitstring2[i]=0;
bitstringunion[i]=0;
bitstringintersection[i]=0;
bitstringdifference[i]=0;
}
generatebitstring(set1,set1_size,bitstring1);
generatebitstring(set2,set2_size,bitstring2);
printf("\n Bit string representation of set1: ");
printset(bitstring1,universalset_size);
printf("\n Bit string representation of set2: ");
printset(bitstring2,universalset_size);
isbitstring_ready=1;
}

void generatebitstring(int arr[],int size,int bitstringarray[]){
int i;
for(i=0;i<size;i++){
int pos=search(universal_set,universalset_size,arr[i]);
if(pos>=0){
bitstringarray[pos]=1;
}
}
}

int checkbitstringstatus(){
if(isbitstring_ready==0){
printf("\n Generate the bit string first!!!");
return 0;
}
return 1;
}

int search(int arr[],int size,int elem){
int i;
for(i=0;i<size;i++){
if(arr[i]==elem)
return i;
}
return -1;
}

void printset(int arr[],int size){
int i;
printf("{");
for(i=0;i<size;i++){
printf("%d",arr[i]);
if(i!=size-1)
printf(",");
}
printf("}");
}

void set_union(int arr1[],int arr2[]){
for(int i=0;i<universalset_size;i++){
bitstringunion[i]=arr1[i]|arr2[i];
}
}

void set_intersection(int arr1[],int arr2[]){
for(int i=0;i<universalset_size;i++){
bitstringintersection[i]=arr1[i]&arr2[i];
}
}

void set_difference(int arr1[],int arr2[]){
for(int i=0;i<universalset_size;i++){
bitstringdifference[i]=arr1[i]&(!arr2[i]);
}
}

int main(){
int choice;
printf("\n 1.Enter universal set \n 2.Enter set 1  \n 3.Enter set 2  \n 4.Bit string representation \n 5.Perform Union \n 6.Perform intersection \n 7.Perform difference \n 8.Exit");
while(1){
printf("\n Enter your choice: ");
scanf("%d",&choice);
switch(choice){
case 1:printf("\n Enter the universal set size(should be less than max size): ");
scanf("%d",&universalset_size);
getset(universal_set,universalset_size);
break;
case 2:printf("\n Enter the set1 size(should be less than or equal to size of universal set): ");
scanf("%d",&set1_size);
getset(set1,set1_size);
break;
case 3:printf("\n Enter the set2 size(should be less than or equal to size of universal set): ");
scanf("%d",&set2_size);
getset(set2,set2_size);
break;
case 4:printf("\n Bit string representation:  ");
printbitstring();
break;
case 5:printf("\n Perform Union: ");
if(checkbitstringstatus()==1){
set_union(bitstring1,bitstring2);
printset(bitstringunion,universalset_size);
}
break;
case 6:printf("\n Perform Intersection: ");
if(checkbitstringstatus()==1){
set_intersection(bitstring1,bitstring2);
printset(bitstringintersection,universalset_size);
}
break;
case 7:printf("\n Perform difference(set1-set2): ");
if(checkbitstringstatus()==1){
set_difference(bitstring1,bitstring2);
printset(bitstringdifference,universalset_size);
}
printf("\n Perform difference(set2-set1): ");
if(checkbitstringstatus()==1){
set_difference(bitstring2,bitstring1);
printset(bitstringdifference,universalset_size);
}
break;
case 8:exit(0);break;
default:printf("\n Invalid choice");
}
}
}

